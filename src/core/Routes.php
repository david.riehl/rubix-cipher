<?php
namespace Rubix\core;

class Routes
{
    private static $routes = null;
    private static $controller_routes = [];

    private static function setRoutes() {
        $json = file_get_contents("src/routes.json");
        $routes = json_decode($json);
        foreach($routes as $route) {
            if (! property_exists($route, "action")) {
                $route->action = null;
            }
            self::$routes[] = new Route($route->path, $route->controller, $route->action);
        }
    }

    public static function getRoutes() {
        if (self::$routes == null) { self::setRoutes(); }
        return self::$routes;
    }

    private static function setControllerRoutes($controller) {
        $json = file_get_contents("src/controllers/${controller}/routes.json");
        $routes = json_decode($json);
        foreach($routes as $route) {
            self::$controller_routes[$controller][] = new Route($route->path, $route->controller, $route->action);
        }
    }

    public static function getControllerRoutes($controller) {
        if (!array_key_exists($controller, self::$controller_routes)) { self::setControllerRoutes($controller); }
        return self::$controller_routes[$controller];
    }

}