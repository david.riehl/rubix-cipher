<?php
namespace Rubix\core;

class Dispatcher
{
    private static $route = null;
    private static $callback = null;
    private static $param_arr = null;

    private static function setRoute()
    {
        $uri_path = Uri::getPath();
        $routes = Routes::getRoutes();
        $route_count = count($routes);
        $found = false;

        for($route_index = 0; ! $found && $route_index < $route_count; $route_index++) {
            $route = $routes[$route_index];
            $route_path = $route->getPath();
            $uri_substr = substr($uri_path, 0, strlen($route_path));
            $uri_substr_next_char = substr($uri_path, strlen($route_path), 1);

            $class = "Rubix\\controllers\\" . $route->getController() . "\\Controller";
            $method = $route->getAction() . "Action";
            $callback = [$class, $method];

            $param_arr = [];
            if($uri_path == $route_path) {
                // match
                // no parameter

                if ($route->getAction() != null) {
                    // defined action
                    self::$route = $route;
                    self::$callback = $callback;
                    self::$param_arr = $param_arr;
                    $found = true;
                } else {
                    // look at controller routes
                    $found = self::setControllerRoute($route->getController());
                }
                
            } else if($uri_substr == $route_path && $uri_substr_next_char == "/") {
                // match
                // with parameters

                $uri_param = substr($uri_path, strlen($route_path) + 1);
                if ($uri_param != "") {
                    $param_arr = explode("/", $uri_param);
                }

                if ($route->getAction() != null) {
                    // defined action
                    self::$route = $route;
                    self::$callback = $callback;
                    self::$param_arr = $param_arr;
                    $found = true;
                } else {
                    // look at controller routes
                    $found = self::setControllerRoute($route->getController());
                }
            }
        }
    }

    private static function setControllerRoute($controller)
    {
        $uri_path = Uri::getPath();
        $routes = Routes::getControllerRoutes($controller);
        $route_count = count($routes);
        $found = false;

        for($route_index = 0; $route_index < $route_count && ! $found; $route_index++) {
            $route = $routes[$route_index];
            $route_path = $route->getPath();
            $uri_substr = substr($uri_path, 0, strlen($route_path));
            $uri_substr_next_char = substr($uri_path, strlen($route_path), 1);

            $class = "Rubix\\controllers\\" . $route->getController() . "\\Controller";
            $method = $route->getAction() . "Action";
            $callback = [$class, $method];
            
            $param_arr = [];
            if($uri_path == $route_path) {
                // Match
                // No parameter

                self::$route = $route;
                self::$callback = $callback;
                self::$param_arr = $param_arr;
                $found = true;

            } else if($uri_substr == $route_path && $uri_substr_next_char == "/") {
                // Match
                // With parameters

                $uri_param = substr($uri_path, strlen($route_path) + 1);
                if ($uri_param != "") {
                    $param_arr = explode("/", $uri_param);
                }

                self::$route = $route;
                self::$callback = $callback;
                self::$param_arr = $param_arr;
                $found = true;

            }
        }

        return $found;
    }

    public static function getRoute() {
        return self::$route;
    }

    public static function dispatch() 
    {
        self::setRoute();
        if (self::$callback != null) {
            call_user_func_array(self::$callback, self::$param_arr);
        }
    }
}