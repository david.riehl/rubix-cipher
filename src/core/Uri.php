<?php
namespace Rubix\core;

class Uri
{
    private static $uri = null;
    private static $root = null;
    private static $path = null;
 
    private static function setUri() {
        self::$uri = $_SERVER['REQUEST_URI'];
    }
    
    public static function getUri(){
        if (self::$uri == null) { self::setUri(); }
        return self::$uri;
    }

    private static function setRoot() {
        self::$root = substr($_SERVER['PHP_SELF'], 0, strlen($_SERVER['PHP_SELF']) - strlen("/index.php"));
    }
    
    public static function getRoot(){
        if (self::$root == null) { self::setRoot(); }
        return self::$root;
    }

    private static function setPath() {
        $path = substr(self::getUri(), strlen(self::getRoot()));
        $path_arr = explode("?", $path);
        self::$path = $path_arr[0];
    }

    public static function getPath() {
        if (self::$path == null) { self::setPath(); }
        return self::$path;
    }
}