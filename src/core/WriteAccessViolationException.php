<?php
namespace Rubix\core;

use Exception;

class WriteAccessViolationException extends Exception {}