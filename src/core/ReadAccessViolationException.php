<?php
namespace Rubix\core;

use Exception;

class ReadAccessViolationException extends Exception {}