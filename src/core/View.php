<?php
namespace Rubix\core;

class View
{
    private static $params = [];
    private static $default_index = "index.php";

    public static function setDefaultIndex($filename) { self::$default_index = $filename; }
    public static function addParameter($name, $value) { self::$params[$name] = $value; }
    public static function display()
    {
        // load user parameters
        foreach(self::$params as $param => $value) {
            $$param = $value;
        }

        // load core parameters
        $default_index = self::$default_index;
        $template = Dispatcher::getRoute()->getController();
        $main = "src/view/templates/${default_index}";

        // call core template
        require_once $main;
    }
}