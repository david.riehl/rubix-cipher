<?php
namespace Rubix\core;

class Entity
{
    public function __get($name)
    {
        $class = get_class($this);
        $property = $name;

        if (! property_exists($class, $property)) {
            $message = "Property ${property} not found in ${class}.";
            throw new PropertyNotFoundExcetption($message);
        }

        $prefix = "get";
        $firstLetter = strtoupper(substr($property, 0, 1));
        $otherLetters = substr($property, 1);
        $method_name = $prefix . $firstLetter . $otherLetters;

        if (! method_exists($this, $method_name)) {
            $message = "Access violation getting property ${property} in ${class}: get method not defined.";
            throw new ReadAccessViolationException($message);
        }

        return $this->$method_name();
    }

    public function __set($name, $value)
    {
        $class = get_class($this);
        $property = $name;

        if (! property_exists($class, $property)) {
            $message = "Property ${property} not found in ${class}.";
            throw new PropertyNotFoundExcetption($message);
        }

        $prefix = "set";
        $firstLetter = strtoupper(substr($property, 0, 1));
        $otherLetters = substr($property, 1);
        $method_name = $prefix . $firstLetter . $otherLetters;

        if (! method_exists($this, $method_name)) {
            $message = "Access violation setting property ${property} in ${class}: set method not defined.";
            throw new WriteAccessViolationException($message);
        }

        $this->$method_name($value);
    }
}