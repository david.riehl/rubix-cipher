function reset_click() {
    document.getElementById('file_input').value = "";

    document.getElementById('txtInput').value = "";
    document.getElementById('txtInputKey').value = "";
    document.getElementById('txtInputSequence').value = "";
    document.getElementById('txtOutput').value = "";
    document.getElementById('txtOutputKey').value = "";
    document.getElementById('txtOutputSequence').value = "";

    document.getElementById('txtInput').focus();
}

function method_click(button, value){
    var hiddenMethod = document.getElementById('hiddenMethod');
    var inputParameters = document.getElementById('inputParameters');
    var outputParameters = document.getElementById('outputParameters');
    var cipherButton = document.getElementById('cipherButton');
    var decipherButton = document.getElementById('decipherButton');
    hiddenMethod.value = value;
    if (value=='cipher') {
        cipherButton.classList.add('active');
        decipherButton.classList.remove('active');
        inputParameters.classList.add('d-none');
        outputParameters.classList.remove('d-none');
    } else {
        decipherButton.classList.add('active');
        cipherButton.classList.remove('active');
        inputParameters.classList.remove('d-none');
        outputParameters.classList.add('d-none');
    }
    reset_click();
}

function execute_click() {
    var hiddenMethod = document.getElementById('hiddenMethod');
    var method = hiddenMethod.value;
    switch (method) {
        case 'cipher':
            ajax_call_cipher();
            break;
        case 'decipher':
            ajax_call_decipher();
            break;
    }
}

function ajax_call_cipher() {
    var txtInput = document.getElementById('txtInput');
    var xhttp = new XMLHttpRequest();
    var root = document.uriRoot;
    xhttp.open("POST", root + "/ajax/cipher", true); 
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Response
            var response = this.responseText;
            var obj = JSON.parse(response);
            //console.log(obj);
            var txtOutput = document.getElementById('txtOutput');
            var txtOutputKey = document.getElementById('txtOutputKey');
            var txtOutputSequence = document.getElementById('txtOutputSequence');

            txtOutput.value = obj.output;
            txtOutputKey.value = obj.key;
            txtOutputSequence.value = obj.sequenceString;
        }
    };
    var data = {input: txtInput.value};
    xhttp.send(JSON.stringify(data));
}

function ajax_call_decipher() {
    var root = document.uriRoot;
    var txtInput = document.getElementById('txtInput');
    var txtInputKey = document.getElementById('txtInputKey');
    var txtInputSequence = document.getElementById('txtInputSequence');
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", root + "/ajax/decipher", true); 
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Response
            var response = this.responseText;
            var obj = JSON.parse(response);
            //console.log(obj);
            var txtOutput = document.getElementById('txtOutput');

            txtOutput.value = obj.output;
        }
    };
    var data = {input: txtInput.value, key: txtInputKey.value, sequence: txtInputSequence.value};
    xhttp.send(JSON.stringify(data));
}

function load_click(e, sender, input_text_id, input_file_id) {
    e = e || window.event;
    e.preventDefault();

    var txtElement = document.getElementById(input_text_id);
    var inputFile = document.getElementById(input_file_id);

    inputFile.click();
}

function file_input_change(e, sender, input_text_id, input_file_id) {
    e = e || window.event;
    e.preventDefault();
    //ajax_post_file(input_text_id, input_file_id);

    var inputFile = document.getElementById(input_file_id);
    var txtElement = document.getElementById(input_text_id);

    let file = inputFile.files[0];
    let reader = new FileReader();
    reader.addEventListener('load', function(e) {
        let text = e.target.result;
        txtElement.value = text;
    });
    reader.readAsText(file);
}

function save_as_click(e, sender, input_text_id) {
    e = e || window.event;
    var txtElement = document.getElementById(input_text_id);
    var value = 'data:text/plain;charset=utf-8,' + encodeURIComponent(txtElement.value);
    sender.setAttribute('href', value);
}

document.onreadystatechange = function() {
    if (document.readyState === 'complete') {
        document.getElementById('txtInput').focus();
    }
};
