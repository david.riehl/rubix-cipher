<form>
    <input type="hidden" id="hiddenMethod" name="method" value="cipher" />
    <div class="form-group row">
        <div class="col-6 mb-3 form-group">
            <label for="methodGroup" class="form-label">Method</label>
            <div id="methodGroup" class="btn-group">
                <button id="cipherButton" class="btn btn-outline-secondary active" type="button" onclick="method_click(this, 'cipher');">Cipher</button>
                <button id="decipherButton" class="btn btn-outline-secondary" type="button" onclick="method_click(this, 'decipher');">Decipher</button>
            </div>
        </div>
        <div class="col-6 mb-3 text-right">
            <button type="button" class="btn btn-outline-secondary" onclick="reset_click();">Reset</button>
        </div>
    </div>
    <div class="mb-3 row">
        <div class="col-6">
            <label for="txtInput" class="form-label">Input</label>
        </div>
        <div class="col-6 text-right">
            <a href="#" class="btn btn-sm btn-outline-secondary" onclick="load_click(event, this, 'txtInput', 'file_input');">Load ...</a>
            <input type="file" id="file_input" class="d-none" onchange="file_input_change(event, this, 'txtInput', 'file_input');" />
        </div>
        <div class="col-12">
            <textarea class="form-control" id="txtInput" name="input" rows="3"></textarea>
        </div>
    </div>
    <div id="inputParameters" class="d-none">
        <div class="mb-3 row">
            <div class="col-6">
                <label for="txtInputKey" class="form-label">Key</label>
            </div>
            <div class="col-6 text-right">
                <a href="#" class="btn btn-sm btn-outline-secondary" onclick="load_click(event, this, 'txtInputKey', 'file_input_key');">Load ...</a>
                <input type="file" id="file_input_key" class="d-none" onchange="file_input_change(event, this, 'txtInputKey', 'file_input_key');" />
            </div>
            <div class="col-12">
                <input class="form-control" id="txtInputKey" name="key"/>
            </div>
        </div>
        <div class="mb-3 row">
            <div class="col-6">
                <label for="txtInputSequence" class="form-label">Sequence</label>
            </div>
            <div class="col-6 text-right">
                <a href="#" class="btn btn-sm btn-outline-secondary" onclick="load_click(event, this, 'txtInputSequence', 'file_input_sequence');">Load ...</a>
                <input type="file" id="file_input_sequence" class="d-none" onchange="file_input_change(event, this, 'txtInputSequence', 'file_input_sequence');" />
            </div>
            <div class="col-12">
                <input type="number" class="form-control" id="txtInputSequence" name="sequence"/>
            </div>
        </div>
    </div>
    <div class="mb-3 text-right">
        <button type="button" class="btn btn-outline-primary" onclick="execute_click();">Execute</button> 
    </div>
    <div class="mb-3">
        <label for="txtOutput" class="form-label">Output</label>
        <textarea class="form-control" id="txtOutput" name="output" rows="3" readonly></textarea>
    </div>
    <div class="mb-3 text-right">
        <a href="#" target="_blank" download="cipher.msg" class="btn btn-outline-secondary" onclick="save_as_click(event, this, 'txtOutput');">Save as...</a> 
    </div>
    <div id="outputParameters">
        <div class="mb-3">
            <label for="txtOutputKey" class="form-label">Key</label>
            <input class="form-control" id="txtOutputKey" name="key" readonly/>
        </div>
        <div class="mb-3 text-right">
            <a href="#" target="_blank" download="cipher.key" class="btn btn-outline-secondary" onclick="save_as_click(event, this, 'txtOutputKey');">Save as...</a> 
        </div>
        <div class="mb-3">
            <label for="txtOutputSequence" class="form-label">Sequence</label>
            <input type="number" class="form-control" id="txtOutputSequence" name="sequence" readonly/>
        </div>
        <div class="mb-3 text-right">
            <a href="#" target="_blank" download="cipher.seq" class="btn btn-outline-secondary" onclick="save_as_click(event, this, 'txtOutputSequence');">Save as...</a> 
        </div>
    </div>
</form>
