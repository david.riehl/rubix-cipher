<?php
namespace Rubix\view\templates;
use Rubix\core\Uri;
?><!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- IE -->
        <link rel="shortcut icon" type="image/svg+xml" href="<?= Uri::getRoot(); ?>/assets/rubixcipher.svg" />
        <!-- other browsers -->
        <link rel="icon" type="image/svg+xml" href="<?= Uri::getRoot(); ?>/assets/rubixcipher.svg" />

        <!-- Bootstrap CSS -->
        <link href="<?= Uri::getRoot(); ?>/vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <title>Rubix Cipher</title>
        <script>document.uriRoot = '<?= Uri::getRoot(); ?>';</script>
<?php if (file_exists("src/view/css/${template}.css")): ?>
        <link href="<?= Uri::getRoot()."/src/view/css/${template}.css"?>" rel="stylesheet">
<?php endif; ?>
    </head>
    <body class="bg-dark">
<?php require_once("navbar.php"); ?>
        <main class="main container bg-light pt-3 pb-1 mb-3" style="margin-top: 5em;">
<?php require_once($template.".php"); ?>
<?php if (file_exists("src/view/js/${template}.js")): ?>
        <script src="<?= Uri::getRoot()."/src/view/js/${template}.js"?>"></script>
<?php endif; ?>
                <footer>
                        &copy; David RIEHL &lt;david&#x2E;riehl[at]gmail&#x2E;com&gt;
                </footer>
        </main>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="<?= Uri::getRoot(); ?>/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>