<?php
namespace Rubix\view\templates;
use Rubix\core\Uri;
?><nav class="navbar fixed-top navbar-dark bg-dark container">
    <a class="navbar-brand" href="#">
        <img src="<?= Uri::getRoot(); ?>/assets/rubixcipher.svg" width="30" height="30" class="d-inline-block align-top" alt="">
        Rubix Cipher
    </a>
</nav>
