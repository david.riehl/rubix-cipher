<?php
namespace Rubix\model;

/**
 * Enum Axis { X, Y, Z }
 */
class Axis
{
    public const X = 0;
    public const Y = 1;
    public const Z = 2;

    /**
     * assert that the value is one of the enum
     * @param $value the value to assert
     */
    public static function assert($value) {
        return assert($value == Axis::X || $value == Axis::Y || $value == Axis::Z);
    }
}