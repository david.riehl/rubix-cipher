<?php
namespace Rubix\model;

class RubixCipher
{
    private $lock;
    private $sequence;
    private $sequence_string;
    private $message;
    private $cube;

    public function __construct()
    { 
        $this->lock = Lock::generate();
        $this->cube = new Cube();
    }

    public function getMessage() { return $this->message; }
    public function setMessage($value) { $this->message = $value; }

    public function getKey() { return base64_encode($this->lock->getKey()); }
    public function setKey($key) { $this->lock->setKey(base64_decode($key)); }

    public function getSequenceString() { return $this->sequence_string; }
    public function setSequenceString($string) { $this->sequence_string = $string; }

    private static function addPaddingTo($message)
    {
        $strlen = strlen($message);
        $cubeRoot = Cube::cubeRoot($strlen);
        // round up to the next int value
        $cubeWidth = Cube::roundUp($cubeRoot);
        $cubeBoxes = pow($cubeWidth, 3);
        $paddingSize = $cubeBoxes - $strlen;
        $padding = str_repeat(" ", $paddingSize);
        $result = $padding . $message;
        return $result;
    }

    public function cipher()
    {
        $this->sequence = Sequence::generate();
        $this->sequence_string = $this->sequence->encode();
        $this->message = self::addPaddingTo($this->message);
        $this->message = $this->lock->cipher($this->message);
        $this->message = base64_encode($this->message);
        $this->cube->write($this->message);
        $this->cube->execute($this->sequence);
        $this->message = $this->cube->read();
    }

    public function decipher()
    {
        $this->sequence = Sequence::decode($this->sequence_string);
        $this->sequence = Sequence::reverse($this->sequence);
        $this->cube->write($this->message);
        $this->cube->execute($this->sequence);
        $this->message = $this->cube->read();
        $this->message = base64_decode($this->message);
        $this->message = $this->lock->decipher($this->message);
        $this->message = trim($this->message);
    }
}