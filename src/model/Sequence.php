<?php
namespace Rubix\model;

use \JsonSerializable;

class Sequence implements JsonSerializable
{
    private $sequence_arr = [];

    public function add($rotation) { $this->sequence_arr[] = $rotation; }
    public function toArray() { return $this->sequence_arr; }
    public function __toString() { return json_encode($this->sequence_arr); }

    public static function generate()
    {
        $sequence = new Sequence();
        for($axis = Axis::X; $axis <= Axis::Z; $axis++) 
        {
            for($depth = Rotation::$DEPHT_MIN; $depth <= Rotation::$DEPHT_MAX; $depth++)
            {
                $number = rand(0, 3);
                $sequence->add(new Rotation($axis, $depth, $number));
            }
        }
        return $sequence;
    }

    public static function reverse($sequence)
    {
        $reversed = new Sequence();
        $array = $sequence->toArray();
        $count = count($array);
        for($i = $count - 1; $i >= 0; $i--) {
            $rotation = $array[$i];
            $reverse = (4 - $rotation->getNumber()) % 4;
            $rotation->setNumber($reverse);
            $reversed->add($rotation);
        }

        return $reversed;
    }

    public function jsonSerialize() {
        return $this->sequence_arr;
    }

    public function encode() {
        $str = "";
        foreach($this->sequence_arr as $rotation) {
            $str .= $rotation->encode();
        }
        return $str;
    }

    public static function decode(string $string):Sequence {
        $sequence = new Sequence();
        $len = strlen($string);
        for($index = 0; $index < $len; $index+= 3) {
            $rotation_str = substr($string, $index, 3);
            $rotation = Rotation::decode($rotation_str);
            $sequence->add($rotation);
        }
        return $sequence;
    }
}