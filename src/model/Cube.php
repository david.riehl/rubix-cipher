<?php
namespace Rubix\model;

class Cube
{
    private $width;
    private $cube_arr;

    public function __construct() {
        $this->width = null;
        $this->cube_arr = [];
    }

    public function getWidth() { return $this->width; }

    public function getValue($x, $y, $z) { return $this->cube_arr[$z][$y][$x]; }
    public function setValue($x, $y, $z, $value) { $this->cube_arr[$z][$y][$x] = $value; }

    public function getCube() { return $this->cube_arr; }
    public function setCube($array) { $this->cube_arr = $array; }

    public function __toString() {
        $json = "[";
        for($z = 0; $z < $this->width; $z++) {
            $json .= json_encode($this->cube_arr[$z]);
            if ($z < $this->width - 1) { $json .= ","; }
        }
        $json .= "]";
        return $json;
    }

    public static function cubeRoot($value) {
        $result = pow($value,(1.0/3.0));
        return $result;
    }

    public static function roundUp($value) {
        return (is_int($value))?$value:intval($value) + 1;
    }

    private function getXaxisSlice($depth) {
        $array = [];
        $x = $depth;
        for($y = 0; $y < $this->width; $y++) {
            for($z = 0; $z < $this->width; $z++) {
                $array[$y][abs($z - $this->width + 1)] = $this->cube_arr[$z][$y][$x];
            }
        }
        return new Slice($this->width, $array);
    }

    private function setXaxisSlice($depth, $slice) {
        $array = $slice->toArray();
        $x = $depth;
        for($y = 0; $y < $this->width; $y++) {
            for($z = 0; $z < $this->width; $z++) {
                $this->cube_arr[$z][$y][$x] = $array[$y][abs($z - $this->width + 1)];
            }
        }
    }

    private function getYaxisSlice($depth) {
        $array = [];
        $y = $depth;
        for($z = 0; $z < $this->width; $z++) {
            for($x = 0; $x < $this->width; $x++) {
                $array[$z][abs($x - $this->width + 1)] = $this->cube_arr[$z][$y][$x];
            }
        }
        return new Slice($this->width, $array);
    }

    private function setYaxisSlice($depth, $slice) {
        $array = $slice->toArray();
        $y = $depth;
        for($z = 0; $z < $this->width; $z++) {
            for($x = 0; $x < $this->width; $x++) {
                $this->cube_arr[$z][$y][$x] = $array[$z][abs($x - $this->width + 1)];
            }
        }
    }

    private function getZaxisSlice($depth) {
        $array = [];
        $z = $depth;
        for($y = 0; $y < $this->width; $y++) {
            for($x = 0; $x < $this->width; $x++) {
                $array[$y][$x] = $this->cube_arr[$z][$y][$x];
            }
        }
        return new Slice($this->width, $array);
    }

    private function setZaxisSlice($depth, $slice) {
        $array = $slice->toArray();
        $z = $depth;
        for($y = 0; $y < $this->width; $y++) {
            for($x = 0; $x < $this->width; $x++) {
                $this->cube_arr[$z][$y][$x] = $array[$y][$x];
            }
        }
    }

    public function rotate($axis = Axis::X, $depth = 0, $number = 1) {

        switch($axis) {
            case Axis::X:
                $slice = $this->getXaxisSlice($depth);
                break;
            case Axis::Y:
                $slice = $this->getYaxisSlice($depth);
                break;
            case Axis::Z:
                $slice = $this->getZaxisSlice($depth);
                break;
        }

        $slice->rotate($number);

        switch($axis) {
            case Axis::X:
                $this->setXaxisSlice($depth, $slice);
                break;
            case Axis::Y:
                $this->setYaxisSlice($depth, $slice);
                break;
            case Axis::Z:
                $this->setZaxisSlice($depth, $slice);
                break;
        }
    }

    public function read() {
        $string = "";
        for($z = 0; $z < $this->width; $z++) {
            for($y = 0; $y < $this->width; $y++) {
                for($x = 0; $x < $this->width; $x++) {
                    $string .= $this->cube_arr[$z][$y][$x];
                }
            }
        }
        return $string;
    }

    public function write($string) {
        $len = strlen($string);
        $cubeRoot = self::cubeRoot($len);
        // round up to the next int value
        $this->width = self::roundUp($cubeRoot);

        for($z = 0; $z < $this->width; $z++) {
            for($y = 0; $y < $this->width; $y++) {
                for($x = 0; $x < $this->width; $x++) {
                    $i = $z * $this->width * $this->width + $y * $this->width + $x;
                    if ($i < $len) {
                        $this->cube_arr[$z][$y][$x] = substr($string, $i, 1);
                    } else {
                        $this->cube_arr[$z][$y][$x] = " ";
                    }
                }
            }
        }
        return $string;
    }

    public function execute($sequence) {
        // echo "==================== EXECUTE ====================\n";
        // var_dump($sequence->encode());
        foreach($sequence->toArray() as $rotation) {
            $this->rotate($rotation->getAxis(), $rotation->getDepth(), $rotation->getNumber());
        }
    }
}