<?php
namespace Rubix\model;

use \JsonSerializable;

class Rotation implements JsonSerializable
{
    public static $DEPHT_MIN = 0;
    public static $DEPHT_MAX = 2;
    private $axis;
    private $depth;
    private $number;

    public function __construct($axis = Axis::X, $depth = 0, $number = 0)
    {
        $this->axis = $axis;
        $this->depth = $depth;
        $this->number = $number;
    }

    public function getAxis() { return $this->axis; }
    public function setAxis($value) 
    { 
        Axis::assert($value);
        $this->axis = $value;
    }

    public function getDepth() { return $this->depth; }
    public function setDepth($value) 
    { 
        assert(self::$DEPHT_MIN <= $value && $value <= self::$DEPHT_MAX);
        $this->depth = $value;
    }

    public function getNumber() { return $this->number; }
    public function setNumber($value) 
    {
        $value = $value % 4;
        $this->number = $value;
    }

    public static function generate() {
        $axis = rand(Axis::X, Axis::Z);
        $depth = rand(self::$DEPHT_MIN, self::$DEPHT_MAX);
        $number = rand(0, 3);
        return new Rotation($axis, $depth, $number);
    }

    public function __toString()
    {
        return json_encode(get_object_vars($this));
    }

    public function jsonSerialize() {
        $obj = (Object)[
            "axis" => $this->axis,
            "depth" => $this->depth,
            "number" => $this->number,
        ];
        return $obj;
    }

    public function encode() {
        return $this->axis . $this->depth . $this->number;
    }

    public static function decode($rotation_str) : Rotation {
        $axis = substr($rotation_str, 0, 1);
        $depth = substr($rotation_str, 1, 1);
        $number = substr($rotation_str, 2, 1);
        $rotation = new Rotation($axis, $depth, $number);
        return $rotation;
    }
}