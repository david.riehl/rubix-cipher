<?php
namespace Rubix\model;

class Lock
{
    public const GENERATED_KEY_SIZE = 10240;
    private const DICTIONARY = "0123456789ABCDEF";
    
    private $key;

    public function getKey() { return $this->key; }
    public function setKey($value) { $this->key = $value; }

    public function cipher($message)
    {
        $str_arr = str_split($message);
        $dictionary_length = strlen(self::DICTIONARY);
        $result = "";
        foreach($str_arr as $index => $char) {
            $key_index = $index % $dictionary_length;
            $key_char = substr($this->key, $key_index, 1);
            $ciphered_char = chr(((ord($char) + intval($key_char, 16)) % 255));
            $result .= $ciphered_char;
        }
        return $result;
    }
    
    public function decipher($message)
    {
        $str_arr = str_split($message);
        $dictionary_length = strlen(self::DICTIONARY);
        $result = "";
        foreach($str_arr as $index => $char) {
            $key_index = $index % $dictionary_length;
            $key_char = substr($this->key, $key_index, 1);
            $ciphered_char = chr(((ord($char) - intval($key_char, 16))) % 255);
            $result .= $ciphered_char;
        }
        return $result;
    }

    public static function generate()
    {
        $lock = new Lock();
        $lock->key = "";
        $lenght = strlen(self::DICTIONARY);
        for($i = 0; $i < self::GENERATED_KEY_SIZE; $i++)
        {
            $offset = rand(0, $lenght - 1);
            $char = substr(self::DICTIONARY, $offset, 1);
            $lock->key .= $char;
        }
        //var_dump($lock->key);
        return $lock;
    }

}