<?php
namespace Rubix\model;

use JsonSerializable;
use Rubix\core\Entity;

class Message extends Entity implements JsonSerializable
{
    private $input;
    private $output;
    private $key;
    private $sequenceString;

    public function getInput() { return $this->input; }
    public function setInput($value) { $this->input = $value; }

    public function getOutput() { return $this->output; }
    public function setOutput($value) { $this->output = $value; }

    public function getKey() { return $this->key; }
    public function setKey($value) { $this->key = $value; }

    public function getSequenceString() { return $this->sequenceString; }
    public function setSequenceString($value) { $this->sequenceString = $value; }

    public function jsonSerialize() {
        return get_object_vars($this);
    }
}