<?php
namespace Rubix\model;

class Slice
{
    private $width;
    private $slice_arr;

    public function __construct($width, $array = []) {
        $this->width = $width;
        $this->slice_arr = $array;
    }

    public function doEmptyInit() {
        for($y = 0; $y < $this->width; $y++) {
            for($x = 0; $x < $this->width; $x++) {
                $this->slice_arr[$y][$x] = "_";
            }
        }
    }

    public function getValue($x, $y) { return $this->slice_arr[$y][$x]; }
    public function setValue($x, $y, $value) { $this->slice_arr[$y][$x] = $value; }

    public function toArray() { return $this->slice_arr; }
    public function setSlice($array) { $this->slice_arr = $array; }

    public function rotate($number = 1) {
        $number = $number % 4;
        $new_slice = [];
        for($i = 0; $i < $number; $i++) {
            for($y = 0; $y < $this->width; $y++) {
                for($x = 0; $x < $this->width; $x++) {
                    $y2 = $x;
                    $x2 = abs($y - $this->width + 1);
                    $new_slice[$y2][$x2] = $this->slice_arr[$y][$x];
                }
            }
            $this->slice_arr = $new_slice;
        }
    }

    public function __toString() {
        return json_encode($this->slice_arr);
    }

    public function display() {
        for($y = 0; $y < $this->width; $y++) {
            $line="|";
            for($x = 0; $x < $this->width; $x++) {
                $line .= " " . $this->slice_arr[$y][$x] . " |";
            }
            $line .= "\n";
            echo $line;
        }
    }
}