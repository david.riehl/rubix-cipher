<?php
namespace Rubix\controllers\error;

use Rubix\core\View;

class Controller
{
    public static function pageNotFoundAction() {
        View::addParameter("error_title", "404");
        View::addParameter("error_message", "Page not found.");
        View::display();
    }
}