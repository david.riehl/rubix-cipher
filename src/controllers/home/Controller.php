<?php
namespace Rubix\controllers\home;

use Rubix\core\View;
use Rubix\model\Message;
use Rubix\model\RubixCipher;

class Controller
{
    public static function homeAction()
    {
        $args = func_get_args();
        View::addParameter("args", $args);
        View::addParameter("method", "cipher");
        View::display();
    }

    public static function ajaxCipherAction()
    {
        $rubix = new RubixCipher();
        $message = new Message();

        $data = json_decode(file_get_contents("php://input"));
        
        $input = $data->input;
        $input = filter_var($input, FILTER_SANITIZE_STRING);

        $rubix->setMessage($input);

        $rubix->cipher();

        $output = $rubix->getMessage();
        $sequenceString = $rubix->getSequenceString();
        $key = $rubix->getKey();

        $message->input = $input;
        $message->output = $output;
        $message->sequenceString = $sequenceString;
        $message->key = $key;
        
        echo json_encode($message);
    }

    public static function ajaxDecipherAction()
    {
        $rubix = new RubixCipher();
        $message = new Message();

        $data = json_decode(file_get_contents("php://input"));
        
        $input = $data->input;
        $key = $data->key;
        $sequenceString = $data->sequence;

        $input = filter_var($input, FILTER_SANITIZE_STRING);
        $key = filter_var($key, FILTER_SANITIZE_STRING);
        $sequenceString = filter_var($sequenceString, FILTER_SANITIZE_STRING);

        $rubix->setMessage($input);
        $rubix->setKey($key);
        $rubix->setSequenceString($sequenceString);
        
        $rubix->decipher();

        $output = $rubix->getMessage();

        $message->input = $input;
        $message->output = $output;
        $message->sequenceString = $sequenceString;
        $message->key = $key;
        
        echo json_encode($message);
    }
}