<?php
namespace Rubix;

use Rubix\core\Dispatcher;

Dispatcher::dispatch();

/*
use Rubix\model\RubixCipher;

echo '<pre>';

$rubix = new RubixCipher();
//$message = strtoupper("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG !");
$message = strtoupper("MESSAGE DE TEST");
var_dump($message);

$rubix->setMessage($message);
$rubix->cipher();
$message = $rubix->getMessage();
$sequence_string = $rubix->getSequenceString();
$key = $rubix->getKey();

var_dump($message);
var_dump($sequence_string);
var_dump($key);

$rubix2 = new RubixCipher();
$rubix2->setMessage($message);
$rubix2->setSequenceString($sequence_string);
$rubix2->setKey($key);
$rubix2->decipher();
$message = $rubix2->getMessage();
var_dump($message);

echo '</pre>';
*/