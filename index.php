<?php
$debug = true;
if ($debug) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    ini_set('assert.exception', 1);
}

require_once('vendor/autoload.php');
require_once('src/index.php');
