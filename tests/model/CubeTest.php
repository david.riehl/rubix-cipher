<?php
namespace RubixTest\model;

use Rubix\model\Cube;
use PHPUnit\Framework\TestCase;
use Rubix\model\Axis;

class CubeTest extends TestCase
{
    protected $cube;

    protected function setUp(): void
    {
        $this->cube = new Cube();
    }

    public function cubeRootProvider()
    {
        return [
            [0, 0],
            [1, 1],
            [2, 8],
            [3, 27],
            [4, 64],
            [5, 125],
        ];
    }

    /**
     * @dataProvider cubeRootProvider
     */
    public function testCubeRoot($expected, $value)
    {
        $actual = Cube::cubeRoot($value);
        $this->assertEquals($expected, $actual);
    }

    private function getCubeArray()
    {
        return [
            [
                [1, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
            [
                [2, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
            [
                [3, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
        ];
    }

    public function rotateProvider()
    {
        $cube_arr = $this->getCubeArray();
        $expected_Z_0 = [
            [
                [0, 0, 1],
                [0, 0, 0],
                [0, 0, 0],
            ],
            [
                [2, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
            [
                [3, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ],
        ];
        return [
            [ $cube_arr, Axis::Z, 0, 1, $expected_Z_0 ]
        ];
    }

    /**
     * @dataProvider rotateProvider
     */
    public function testRotate($cube_arr, $axis, $depth, $number, $expected)
    {
        $this->cube->setCube($cube_arr);
        $this->cube->rotate($axis, $depth, $number);
        $actual = $this->cube->getCube();
        $this->assertEquals($expected, $actual);
    }
}