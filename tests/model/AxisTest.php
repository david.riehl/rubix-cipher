<?php
namespace RubixTest\model;

use AssertionError;
use Rubix\model\Axis;
use PHPUnit\Framework\TestCase;

class AxisTest extends TestCase
{

    public function assertProvider()
    {
        return [
            [Axis::X, true],
            [Axis::Y, true],
            [Axis::Z, true],
        ];
    }

    /**
     * @dataProvider assertProvider
     */
    public function testAssert($value, $expected)
    {
        $this->assertEquals($expected, Axis::assert($value));
    }

    public function assertExceptionProvider()
    {
        return [
            [3, false],
            [-1, false],
        ];
    }

    /**
     * @dataProvider assertExceptionProvider
     */
    public function testAssertException($value, $expected)
    {
        ini_set('assert.exception', 1);
        $this->expectException(AssertionError::class);
        Axis::assert($value);
    }
}